#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 11 22:56:19 2019

@author: szary
"""


import hyperparameters as hp
import utils as ut
import tensorflow as tf

from tensorflow.keras.utils import to_categorical


def create_placeholders(n_h, n_w, n_c, n_y):
    
    # create placeholders
    X = tf.placeholder(dtype = tf.float32, shape = (None, n_h, n_w, n_c))
    Y = tf.placeholder(dtype = tf.float32, shape = (None, n_y))
    
    return X, Y


def initialize_parameters(filter_dimensions):
    
    # initialize weight parameters for convolution layers
    W1 = tf.get_variable('W1', shape = filter_dimensions['W1'])
    W2 = tf.get_variable('W2', shape = filter_dimensions['W2'])
    W3 = tf.get_variable('W3', shape = filter_dimensions['W3'])
    W4 = tf.get_variable('W4', shape = filter_dimensions['W4'])
    
    parameters = {'W1': W1, 'W2': W2, 'W3': W3, 'W4': W4} 
    
    return parameters


def forward_propagation(X, parameters, strides, is_training, dropout_rate):

    # first block
    Z1 = tf.nn.conv2d(X, parameters['W1'], strides['conv'], padding = 'VALID')
    A1 = tf.nn.relu(Z1)
    P1 = tf.nn.max_pool(A1, ksize = strides['pool'], strides = strides['pool'], padding = 'VALID')
    D1 = tf.layers.dropout(P1, rate = dropout_rate, training = is_training)

    # second block
    Z2 = tf.nn.conv2d(D1, parameters['W2'], strides['conv'], padding = 'VALID')
    A2 = tf.nn.relu(Z2)
    P2 = tf.nn.max_pool(A2, ksize = strides['pool'], strides = strides['pool'], padding = 'VALID')
    D2 = tf.layers.dropout(P2, rate = dropout_rate, training = is_training)
    
    # third block
    Z3 = tf.nn.conv2d(D2, parameters['W3'], strides['conv'], padding = 'VALID')
    A3 = tf.nn.relu(Z3)
    P3 = tf.nn.max_pool(A3, ksize = strides['pool'], strides = strides['pool'], padding = 'VALID')
    D3 = tf.layers.dropout(P3, rate = dropout_rate, training = is_training)
    
    # fourth block    
    Z4 = tf.nn.conv2d(D3, parameters['W4'], strides['conv'], padding = 'VALID')
    A4 = tf.nn.relu(Z4)
    P4 = tf.nn.max_pool(A4, ksize = strides['pool'], strides = strides['pool'], padding = 'VALID')
    D4 = tf.layers.dropout(P4, rate = dropout_rate, training = is_training)
    

    # flatten
    F = tf.contrib.layers.flatten(D4)
    
    # dense block
    Z3 = tf.contrib.layers.fully_connected(F, 128, activation_fn = None)
    A3 = tf.nn.relu(Z3)
    D4 = tf.layers.dropout(A3, rate = dropout_rate, training = is_training)

    # output
    Z4 = tf.contrib.layers.fully_connected(D4, 10, activation_fn = None)
    
    return Z4


def model_train(learning_rate, num_epochs, batch_size, print_loss = True):

    # load dataset
    X_train, Y_train, X_val, Y_val, X_test, Y_test = ut.load_dataset(hp.train_split, hp.val_split)
    Y_train = to_categorical(Y_train, num_classes = 10)
    Y_val = to_categorical(Y_val, num_classes = 10)
    Y_test = to_categorical(Y_test, num_classes = 10)
    
    tf.reset_default_graph()
    graph = tf.get_default_graph()    
    with graph.as_default():
        
        # create placeholders
        X, Y = create_placeholders(*hp.placeholder_dimensions)
        
        # for testing purposes
        is_training = tf.placeholder_with_default(True, shape = ())

        # initialize parameters
        parameters = initialize_parameters(hp.filter_dimensions)
    
        # forward propagate
        Z4 = forward_propagation(X, parameters, hp.strides, is_training, dropout_rate = 0.2)
    
        # compute cost
        loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits = Z4, labels = Y))

        # define optimizer for backpropagation that minimizes the cost function 
        optimizer = tf.train.AdamOptimizer(learning_rate).minimize(loss)
        
        # calculate correct predictions 
        prediction = tf.argmax(Z4, 1)
        correct_prediction = tf.equal(prediction, tf.argmax(Y, 1))
    
        # calculate accuracy on test set
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
        
        # create a saver to save the trained models
        saver = tf.train.Saver()
    
    # initialize variables    
    init = tf.global_variables_initializer()
        
    # start session
    with tf.Session() as sess:
                
        sess.run(init)
        
        best_val = 0.
        train_losses = []
        val_losses = []
        train_accs = []
        val_accs = []
        
        for epoch in range(num_epochs):
            
            train_loss = 0.
            train_acc = 0.

            minibatches = ut.random_minibatches(X_train, Y_train, batch_size)
            for batch_X, batch_Y in minibatches:
                _, train_accuracy, temp_loss = sess.run([optimizer, accuracy, loss], feed_dict = {X: batch_X, Y: batch_Y})
                train_loss += temp_loss / len(minibatches)
                train_acc += train_accuracy / len(minibatches)
            
            if print_loss:
                val_acc, val_loss = sess.run([accuracy, loss], feed_dict = {X: X_val, Y: Y_val, is_training: False})
                if val_acc > best_val:
                    best_val = val_acc
                    print('*')
                    saver.save(sess, hp.save_path + 'best.ckpt')
                print('Epoch: %i / %i\nTraining accuracy: %f, Training loss: %f\nValidation accuracy: %f, Validation loss: %f'
                      %(epoch+1, num_epochs, train_accuracy, train_loss, val_acc, val_loss))
                train_losses.append(train_loss)
                train_accs.append(train_acc)
                val_losses.append(val_loss)
                val_accs.append(val_acc)
        
        saver.save(sess, hp.save_path + str(num_epochs) + '.ckpt')
        test_acc = sess.run(accuracy, feed_dict = {X: X_test, Y: Y_test, is_training: False})
        print('Testing accuracy after %i epochs: %f' %(num_epochs, test_acc))
        
        saver.restore(sess, hp.save_path + '100.ckpt')
        test_acc = sess.run(accuracy, feed_dict = {X: X_test, Y: Y_test, is_training: False})
        predictions = sess.run(prediction, feed_dict = {X: X_test})
        
        print('Best validation accuracy: ', best_val)
        print('Testing accuracy: ', test_acc)
        
        ut.plot_predictions(X_test, Y_test, predictions)
        
    # plot the costs
    ut.plot_loss(train_losses, train_accs, learning_rate, val_losses, val_accs)




model_train(hp.learning_rate, hp.num_epochs, hp.batch_size)
