#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 12 23:28:35 2019

@author: szary
"""
# image height, width, num_channels and classes
placeholder_dimensions = (100, 100, 1, 10)

# filter dimensions for conv layers
filter_dimensions = {'W1': [3, 3, 1, 8], 'W2': [3, 3, 8, 16], 'W3': [3, 3, 16, 32], 'W4': [3, 3, 32, 64]}

# strides for conv and max_pool layers
strides = {'conv': [1, 1, 1, 1], 'pool': [1, 2, 2, 1]}

learning_rate = 0.0003
num_epochs = 150
batch_size = 64

train_split = 0.75
val_split = 0.10

save_path = 'best_validation_weights/8_16_32/4block2/'