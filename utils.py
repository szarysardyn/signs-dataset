 #!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 12 22:18:37 2019

@author: szary
"""

import os
import cv2
import numpy as np
import matplotlib.pyplot as plt


def create_dataset():
    dataset_dir = '/home/szary/Python NN/signs dataset/dataset'
    signs_dir = os.listdir(dataset_dir)
    dataset = []
    for sign in signs_dir:
        img_path = os.path.join(dataset_dir, sign)
        img_names = os.listdir(img_path)
        for name in img_names:
            try:
                img_array = cv2.imread(str(img_path + '/' + name), cv2.IMREAD_GRAYSCALE)/255
                dataset.append([img_array.reshape(100, 100, 1), np.array(int(sign)).reshape(1)])
            except:
                continue
            
    np.random.shuffle(dataset)
    
    X_set_, Y_set_ = [], []
    for img, label in dataset:
        X_set_.append(img)
        Y_set_.append(label)

    X_set = np.stack(X_set_)
    Y_set = np.stack(Y_set_)
    
    np.save('X.npy', X_set)
    np.save('Y.npy', Y_set)

def load_dataset(train_split, val_split):
    
    X_set, Y_set = np.load('processed dataset/X.npy'), np.load('processed dataset/Y.npy')
    
    # divide dataset into train and test
    train_split_idx = int(len(Y_set) * train_split)
    val_split_idx = train_split_idx + int(len(Y_set) * val_split)
    
    X_train, Y_train = X_set[:train_split_idx], Y_set[:train_split_idx]
    X_val, Y_val = X_set[train_split_idx: val_split_idx], Y_set[train_split_idx: val_split_idx]
    X_test, Y_test = X_set[val_split_idx:], Y_set[val_split_idx:]
    
    return X_train, Y_train, X_val, Y_val, X_test, Y_test

def random_minibatches(X, Y, batch_size):
    
    minibatches = []
    m = int(Y.shape[0])
    num_batches = int(m / batch_size)
    rest = m - num_batches * batch_size
    
    for i in range(num_batches):
        minibatches.append([X[i*batch_size : (i+1)*batch_size], Y[i*batch_size : (i+1)*batch_size]])
        
    minibatches.append([X[X.shape[0] - rest : X.shape[0]], Y[Y.shape[0] - rest : Y.shape[0]]])
    np.random.shuffle(minibatches)
    
    return minibatches

def plot_loss(train_losses, train_accs, learning_rate, val_losses = None, val_accs = None):
    
    plt.figure(figsize = (9, 4))
    
    plt.subplot(1, 2, 1)
    plt.plot(np.squeeze(train_losses), label = 'train loss')
    if val_losses != None:    
        plt.plot(np.squeeze(val_losses), label = 'val loss')
    plt.xlabel('Iterations')
    plt.ylabel('Loss')
    plt.title('Learning rate = ' + str(learning_rate))
    plt.legend(loc = 'upper right')
    
    plt.subplot(1, 2, 2)
    plt.plot(train_accs, label = 'train acc')
    if val_accs != None:
        plt.plot(val_accs, label = 'val acc')
    plt.xlabel('Iterations')
    plt.ylabel('Accuracy')
    plt.title('Learning rate = ' + str(learning_rate))
    
    plt.legend(loc = 'lower right')
    plt.show()
    
def plot_predictions(features, labels, predictions):
    
    idx = np.random.randint(predictions.shape[0], size = 20)
    plt.figure(figsize = (16, 14))
    
    for i in range(20):
        plt.subplot(4, 5, i+1)
        plt.title('T: ' + str(np.argmax(labels[idx[i]])) + ' P: ' + str(predictions[idx[i]]))
        plt.imshow(features[idx[i]].reshape(100, 100), cmap = 'gray')
    plt.show()
    
def show_filter(array):
    array = array[np.random.randint(0, array.shape[0])]
    plt.figure(figsize = (9, 5))
    
    for i in range(8):
        plt.subplot(2, 4, i+1)
        max_val = np.max(array[:, :, i])
        im = array[:, :, i]/max_val
        plt.imshow(im.reshape(im.shape[:2]), cmap = 'gray')
    plt.show()