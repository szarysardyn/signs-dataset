# MNIST Signs dataset

## Introduction
MNIST Signs dataset consists of 100x100x3 images of hand signs. There were 218 different people participating, each had their hand photographed 10 times, once for each digit representation. Here are some examples from the dataset:
   
|labels|2|8|6|
|:--|:--:|:--:|:--:|
|images|![](/imgs/IMG_4205.JPG) | ![](/imgs/IMG_4444.JPG) | ![](/imgs/IMG_4610.JPG)

## Image Preprocessing
The dataset did not require much preprocessing. Basically, I loaded the grayscale images using OpenCV library into a NumPy array, normalized them by dividing by max pixel value and stacking each image into a 4D array of dimensions

```python
X.shape = [dataset_size, 100, 100, 1]
Y.shape = [dataset_size, 1]
```
Feel free to use this dataset, however remember to one-hot encode the labels. 

## Model architectures
All models were trained over 100 epochs, with cross entropy with logtits loss function, where two best models were saved out of each session. One with **highest validation accuracy** and the other after **whole learning process**. I found that changing learning rate did not affect the learning process significantly. However, **adding dropout** layer after each block was a good choice to prevent overfitting. With a dataset as small as this one, it is quite simple for a neural network to learn random noises in training set and not generalize well on real world images. I also decided to use **Rectified Linear Unit** as an activation function after each layer.

### Convnet with 3 convolution blocks
I used quite simple architecture consisting of 3 convolution blocks, with dropout layer after to prevent overfitting. 

```python
    # first block, convolution with filter size [3, 3, 1, 8] and max_pool with window size and stride of 2
    Z1 = tf.nn.conv2d(X, parameters['W1'], strides['conv'], padding = 'VALID')
    A1 = tf.nn.relu(Z1)
    P1 = tf.nn.max_pool(A1, ksize = strides['pool'], strides = strides['pool'], padding = 'VALID')
    D1 = tf.layers.dropout(P1, rate = 0.2, training = is_training)

    # second block, convolution with filter size [3, 3, 8, 16] 
    Z2 = tf.nn.conv2d(D1, parameters['W2'], strides['conv'], padding = 'VALID')
    A2 = tf.nn.relu(Z2)
    P2 = tf.nn.max_pool(A2, ksize = strides['pool'], strides = strides['pool'], padding = 'VALID')
    D2 = tf.layers.dropout(P2, rate = 0.2, training = is_training)
    
    # third block, convolution with filter size [3, 3, 16, 32]
    Z3 = tf.nn.conv2d(D2, parameters['W3'], strides['conv'], padding = 'VALID')
    A3 = tf.nn.relu(Z3)
    P3 = tf.nn.max_pool(A3, ksize = strides['pool'], strides = strides['pool'], padding = 'VALID')
    D3 = tf.layers.dropout(P3, rate = 0.2, training = is_training)
    
    # flatten
    F = tf.contrib.layers.flatten(D3)
    
    # dense block
    Z3 = tf.contrib.layers.fully_connected(F, 128, activation_fn = None)
    A3 = tf.nn.relu(Z3)
    D4 = tf.layers.dropout(A3, rate = 0.2, training = is_training)

    # output
    Z4 = tf.contrib.layers.fully_connected(D4, 10, activation_fn = None)
```

This model managed to get highest validation accuracy of 89.7%, which is surprising considering it does not have so many parameters on early layers. Here are loss function and accuracy of training and validation sets plotted:

![](/imgs/8_16_32.png)

We can see that the model has slight issues with overfitting problem, even with dropout layers.
### Convnet with different filter sizes
I used the same architecture as above, however i tweaked the filter sizes of each convolution layer:

```python
    # first conv with filter size [3, 3, 1, 16]
    Z1 = tf.nn.conv2d(X, parameters['W1'], strides['conv1'], padding = 'VALID')
    A1 = tf.nn.relu(Z1)
    P1 = tf.nn.max_pool(A1, ksize = strides['pool1'], strides = strides['pool1'], padding = 'VALID')
    D1 = tf.layers.dropout(P1, rate = 0.2, training = is_training)
    
    # second conv with filter size [3, 3, 16, 32]
    ...
    
    # third conv with filter size[3, 3, 32, 64] 
    ...

```

This model was able to get highest validation accuracy of 94.1%, yet the testing accuracy was still not satisfactory with a value of 91%. However it was a lot better than previously used model with less filter features. Here are the plots of this run:

![](/imgs/16_32_64.png)

I also trained two other models wtih same filter sizes but with the dropout rate changed to 0.15 and second with 0.25. The model with decreased dropout performed worse on validation and testing set, with 91.7% and 90% accuracy. The other with increased dropout rate yielded no further improvement than that of the standard one, with validation and testing accuracy of 92.1% and 89.3%.

### Convnet with four convolutional blocks
This time I tried adding another convolution block, and the results so far were the best. The network managed to get 98% validation accuracy and 95.5% accuracy on the testing set.

```python

    # first block, conv filters [3, 3, 8, 16], max_pool window = 2, dropout rate = 0.2
    Z1 = tf.nn.conv2d(X, parameters['W1'], strides['conv'], padding = 'VALID')
    A1 = tf.nn.relu(Z1)
    P1 = tf.nn.max_pool(A1, ksize = strides['pool'], strides = strides['pool'], padding = 'VALID')
    D1 = tf.layers.dropout(P1, rate = dropout_rate, training = is_training)

    # second block, conv filters [3, 3, 16, 32]
    Z2 = tf.nn.conv2d(D1, parameters['W2'], strides['conv'], padding = 'VALID')
    A2 = tf.nn.relu(Z2)
    P2 = tf.nn.max_pool(A2, ksize = strides['pool'], strides = strides['pool'], padding = 'VALID')
    D2 = tf.layers.dropout(P2, rate = dropout_rate, training = is_training)
    
    # third block, conv filters [3, 3, 32, 64]
    Z3 = tf.nn.conv2d(D2, parameters['W3'], strides['conv'], padding = 'VALID')
    A3 = tf.nn.relu(Z3)
    P3 = tf.nn.max_pool(A3, ksize = strides['pool'], strides = strides['pool'], padding = 'VALID')
    D3 = tf.layers.dropout(P3, rate = dropout_rate, training = is_training)
    
    # fourth block, conv filters [3, 3, 64, 128]
    Z4 = tf.nn.conv2d(D3, parameters['W4'], strides['conv'], padding = 'VALID')
    A4 = tf.nn.relu(Z4)
    P4 = tf.nn.max_pool(A4, ksize = strides['pool'], strides = strides['pool'], padding = 'VALID')
    D4 = tf.layers.dropout(P4, rate = dropout_rate, training = is_training)
    

    # flatten
    F = tf.contrib.layers.flatten(D4)
    
    # dense block
    Z3 = tf.contrib.layers.fully_connected(F, 128, activation_fn = None)
    A3 = tf.nn.relu(Z3)
    D4 = tf.layers.dropout(A3, rate = dropout_rate, training = is_training)

    # output
    Z4 = tf.contrib.layers.fully_connected(D4, 10, activation_fn = None)
```

However, this time it was the network after 100 epochs, not the one with best validation accuracy. Plots of loss and accuracy over epochs are following:

![](/imgs/best.png)

After seeing the improvement with more parameters I tried to add more feature detection, thus allowing to spread the filters over four convolution blocks. As we can see in the image above, this was a right decision. Loss function of both training and validation is converging nicely, and the validation accuracy is comparable to training accuracy. This means the model generelizes well on unseen data.

To better visualize the results I plotted the images with their true labels and predicted label by the model. **The images below were not part of the training dataset**.

![](/imgs/predictions.png)

The model missclasified only one image out of the twenty shown above. The testing accuracy was 95% so generally one image out of twenty will be missclasified.  

We can also try and visualise what the model 'sees' after applaying first convolution layer:

![](/imgs/filter.png)

which is then followed by a relu activation and max pooling operation:

![](/imgs/pool.png)

The network was able to learn higher class abstractions such as edge detection, in order to combine it into an output in later stages of the model.

I used the output of the first convolution layer Z1 in the graph, passed the image in sess.run() and plotted the result using following function:

```python
def show_filter(array):
    
    array = array[np.random.randint(0, array.shape[0])]

    plt.figure(figsize = (9, 5))
    
    for i in range(8):
        plt.subplot(2, 4, i+1)
        max_val = np.max(array[:, :, i])
        im = array[:, :, i]/max_val
        plt.imshow(im.reshape(im.shape[:2]), cmap = 'gray')
        
    plt.show()
``` 

## Conclusion
As far as deep learning engineers know, adding more parameters generally result in better performance, but overall training time as well as computational speed is the main issue here. All models above were trained on a single CPU with training time ranging from five to twenty minutes, depending on the model's size. I highly encourage everyone to try and push this dataset to its limit, and achieve even greater testing accuracy. 

